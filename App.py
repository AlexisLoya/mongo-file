from flask import Flask, request, url_for, make_response, redirect, render_template,flash
from flask_pymongo import PyMongo
from flask_bootstrap import Bootstrap
from config import Config

# initializations
app = Flask(__name__)
# MongoDB connection
app.config["MONGO_URI"] = 'mongodb+srv://m001-student:m001-mongodb-basics@sandbox.lmeer.mongodb.net/imagenes'
mongo = PyMongo(app)
Bootstrap(app)
app.config.from_object(Config)

# routes
@app.errorhandler(404)
def not_found(error):
    return render_template('404.html', error=error)


@app.errorhandler(500)
def internal_server_error(error):
    return render_template('500.html', error=error)

@app.route('/')
def Index():
    return make_response(redirect('/upload'))

@app.route('/upload', methods=['GET', 'POST'])
def signup():
    if request.method == 'POST':
        image = request.files['image']
        try:
            mongo.save_file(image.filename, image)
            mongo.db.image.insert_one(
                {'imagen': image})

            return make_response(redirect('/upload'))
        except Exception as e:
            print(e)
            flash('guardado correcto', 'success')
            return make_response(redirect('/upload'))

    return render_template('upload.html')

'''
ssh ec2-user@34.204.17.163 -i C:\Users\alexl\Downloads\key-flask.pem
'''
# starting the app
if __name__ == "__main__":
    app.run(port=80)